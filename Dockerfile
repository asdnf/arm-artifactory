FROM docker pull armv7/armhf-ubuntu

COPY /jfrog-artifactory-oss-5.4.6.zip /
RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y openjdk-8-jdk unzip
RUN unzip jfrog-artifactory-oss-5.4.6.zip

CMD /artifactory-oss-5.4.6/bin/artifactoryctl
